#Third party scraper - Majority of scrapy code taken from https://gist.github.com/vijayanandrp/574e5da2df817ee6f20bfa937ab9b5e9
from pprint import pprint
from flask import Flask
from flask import Response
from flask import request
import flask
import json
import os
app = Flask(__name__)
import subprocess
import scrapy
import re
import unicodedata
from scrapy.crawler import CrawlerProcess
from urllib.parse import urlparse
import pymysql.cursors
import sqlalchemy as db
import logging

logger = logging.getLogger()

@app.route('/')
def getData():
    url = request.args.get('url')
    full_url = 'start_url=https://en.wikipedia.org/wiki/' + url
    print (full_url)
    spider_name = "wiki"
    subprocess.check_output(['scrapy', 'crawl', spider_name, '-a', full_url, '-o', 'output.json'])
    output = {
    "error": False,
    "string": "",
    "answer": url
    }
    j = json.dumps(output)

    connection = pymysql.connect(unix_socket='/cloudsql/assignment-3-264920:europe-west1:conor-sql',
                                 user='root',
                                 password='Baratheon97',
                                 db='AssignmentDB')

    response = flask.Response(j)

    response.headers['Content-Type'] = "application/json"
    response.headers['Access-Control-Allow-Origin'] = "*"

    with open('output.json', 'r') as fin:
        lines = fin.readlines()

    with open('output.json', 'w') as fin:
        for line in lines:
            if line.strip("\n") == "]" or line.strip("\n") == "[":
                continue
            else:
                fin.write(line)

    try:
        with open('output.json', 'r') as fin:
            data = json.load(fin)
    except Exception as e:
        logger.exception(e)
        return Response(status=500, response="Failed")


    Title = data['Title']
    wikiDescription = data['strings']

    os.remove("output.json")

    try:
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO scraper_data(Title, Data) VALUES (%s, %s)", (str(Title), str(wikiDescription)))

        connection.commit()
    except Exception as e:
        logger.exception(e)
        return Response(status=500, response="Failed2")
    finally:
        connection.close()

    return response


if __name__ == '__main__':
    app.run(host='0.0.0.0')


class WikiSpider(scrapy.Spider):
    name = 'wiki'

    def __init__(self, *args, **kwargs):
        super(WikiSpider, self).__init__(*args, **kwargs)
        self.start_urls = [kwargs.get('start_url')]

    def parse(self, response):
        parsed_uri = urlparse(response.url)
        domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)

        def _clean(value):
            value = ' '.join(value)
            value = value.replace('\n', '')
            value = unicodedata.normalize("NFKD", value)
            value = re.sub(r' , ', ', ', value)
            value = re.sub(r' \( ', ' (', value)
            value = re.sub(r' \) ', ') ', value)
            value = re.sub(r' \)', ') ', value)
            value = re.sub(r'\[\d.*\]', ' ', value)
            value = re.sub(r' +', ' ', value)
            return value.strip()

        strings = []
        for i in range(0, 100):
            try:
                for node in response.xpath('//*[@id="mw-content-text"]/div/p[{}]'.format(i)):
                    text = _clean(node.xpath('string()').extract())
                    if len(text):
                        strings.append(text)
            except Exception as error:
                strings.append(str(error))
        info_card = dict()
        i = 0
        rows = response.xpath('//*[@id="mw-content-text"]/div/table[@class="infobox vcard" ]/tr')
        for row in rows:
            # Scraping Image in info box
            value = dict()
            if row.css('.image'):
                if row.css('img'):
                    i += 1
                    item = 'Logo_{}'.format(i)
                    try:
                        value['logo_thumb_url'] = row.css('img').xpath('@src').extract_first().replace('//', '')
                        try:
                            value['logo_url'] = domain + row.css('a::attr(href)').extract_first()[1:]
                            try:
                                text = _clean(row.xpath('string()').extract())
                                if text:
                                    value['text'] = text
                            except:
                                pass
                        except:
                            pass
                    except:
                        pass

            # Scraping info box values
            elif row.xpath('th'):
                item = row.xpath('th//text()').extract()
                item = [_.strip() for _ in item if _.strip() and _.replace('\n', '')]
                item = ' '.join(item)
                item = item.replace('\n', '')
                item = unicodedata.normalize("NFKD", item)
                item = re.sub(r' +', ' ', item)
                item = item.strip()

                if row.xpath('td/div/ul/li'):
                    value = []
                    for li in row.xpath('td/div/ul/li'):
                        value.append(''.join(li.xpath('.//text()').extract()))
                    value = [_.strip() for _ in value if _.strip() and _.replace('\n', '')]
                    value = ', '.join(value)
                else:
                    value = row.xpath('td//text()').extract()
                    value = [_.strip() for _ in value if _.strip() and _.replace('\n', '')]

                    if item == 'Website':
                        value = ''.join(value)
                    else:
                        value = ' '.join(value)

                value = value.replace('\n', '')
                value = unicodedata.normalize("NFKD", value)
                value = re.sub(r' , ', ', ', value)
                value = re.sub(r' \( ', ' (', value)
                value = re.sub(r' \) ', ') ', value)
                value = re.sub(r' \)', ') ', value)
                value = re.sub(r'\[\d\]', ' ', value)
                value = re.sub(r' +', ' ', value)
                value = value.strip()
                info_card[item] = value

        return {
            'Title': response.css('#firstHeading::text').extract_first(),
            'strings': strings
        }
